import javafx.util.Pair;
import java.util.ArrayList;
import java.util.Collections;

public class Graph {
    private ArrayList<Pair<Integer,Integer>> edges;
    private ArrayList<Node> nodes;
    private int V;
    private int E;

    public Graph(ArrayList<Node> nodes, ArrayList<Pair<Integer,Integer>> edges, int V, int E){
        this.nodes = nodes;
        this.edges = edges;
        this.V = V;
        this.E = E;
    }

    public int MRV(){
        int minimDomainRemain = Integer.MAX_VALUE;
        int chosenNodeNumber = -1;
        for(int i=0 ; i<V ; i++){
            if (nodes.get(i).numberDomainRemain()<minimDomainRemain){
                chosenNodeNumber = i;
                minimDomainRemain = nodes.get(i).numberDomainRemain();
            }
        }
        return chosenNodeNumber;
    }

    public int normal(){
        for(int i=0 ; i<V ; i++){
            if (!nodes.get(i).isAssignValue()){
                return i;
            }
        }
        return -1;
    }

    public void FC(int var){
        FCNode(var);
        ArrayList<Integer> neighbour = neighbour(var);
        for (Integer nNumber: neighbour){
            if (nodes.get(nNumber).isAssignValue()){
                FCNode(nNumber);
            }
            else{
                ArrayList<Integer> nDomain = nodes.get(nNumber).getDomain();
                ArrayList<Integer> nDomainNew = new ArrayList<>();
                for (Integer value: nDomain){
                    Graph fcGraph = getCopy();
                    fcGraph.assignVar(nNumber,value);
                    fcGraph.FCNode(nNumber);
                    if (fcGraph.isConsistent()){
                        nDomainNew.add(value);
                    }
                }
                nodes.get(nNumber).setDomain(nDomainNew);
            }
        }
    }

    public void FCNode(int var){
        if (nodes.get(var).getType()=='C'){
            return;
        }
        ArrayList<Integer> fcNeighbour = neighbour(var);

        ArrayList<Integer>[] newDomain = new ArrayList[fcNeighbour.size()];
        int values[] = new int[fcNeighbour.size()];
        for(int i=0 ; i<fcNeighbour.size() ; i++){
            newDomain[i] = new ArrayList<Integer>();
        }
        for (int i=0 ; i<fcNeighbour.size() ; i++){
            if (nodes.get(i).isAssignValue()){
                values[i] = nodes.get(fcNeighbour.get(i)).getValue();
            }
            else{
                values[i] = -1;
            }
        }

        checkFC(newDomain,fcNeighbour,values,var);
        for (int i=0 ; i<fcNeighbour.size() ; i++){
            if (!nodes.get(fcNeighbour.get(i)).isAssignValue()){
                nodes.get(fcNeighbour.get(i)).setDomain(newDomain[i]);
            }
        }
    }

    public void checkFC(ArrayList<Integer>[] newDomain, ArrayList<Integer> neighbour, int[] values,int var){
        for(int i=0 ; i<neighbour.size() ; i++){
            if (values[i]==-1){
                for (Integer v : nodes.get(neighbour.get(i)).getDomain()){
                    values[i] = v;
                    checkFC(newDomain,neighbour,values,var);
                }
                values[i] = -1;
                return;
            }
        }
        if (checkConstraint(neighbour.size(),values,var)){
            for (int i=0 ; i<neighbour.size() ; i++){
                if (!newDomain[i].contains(values[i])){
                    newDomain[i].add(values[i]);
                }
            }
        }
        return;
    }

    public boolean checkConstraint(int numberNeighbour, int[] values, int var){
        int result,mrd;
        switch (nodes.get(var).getType()){
            case 'C':
                return true;
            case 'T':
                result = 1;
                for(int i=0 ; i<numberNeighbour ; i++){
                    result*= values[i];
                }
                mrd = 0;
                while (result>0){
                    mrd = result%10;
                    result /= 10;
                }
                return nodes.get(var).getValue() == mrd;
            case 'S':
                result = 1;
                for(int i=0 ; i<numberNeighbour ; i++){
                    result*= values[i];
                }
                result = result%10;

                return nodes.get(var).getValue() == result;
            case 'P':
                result = 0;
                for(int i=0 ; i<numberNeighbour ; i++){
                    result+= values[i];
                }
                mrd = 0;
                while (result>0){
                    mrd = result%10;
                    result /= 10;
                }

                return nodes.get(var).getValue() == mrd;
            case 'H':
                result = 0;
                for(int i=0 ; i<numberNeighbour ; i++){
                    result+= values[i];
                }
                result %= 10;
                return nodes.get(var).getValue() == result;
        }
        return false;
    }

    public ArrayList<Integer> neighbour(int nodeNumber){
        ArrayList<Integer> neighbour = new ArrayList<>();
        for(int i= 0 ; i<E ; i++){
            if (edges.get(i).getValue().equals(nodeNumber)){
                neighbour.add(edges.get(i).getKey());
            }
            if (edges.get(i).getKey().equals(nodeNumber)){
                neighbour.add(edges.get(i).getValue());
            }
        }
        Collections.sort(neighbour);
        return neighbour;
    }

    public ArrayList<Integer> domainVar(int var){
        return nodes.get(var).getDomain();
    }

    public void assignVar(int var, int value){
        nodes.get(var).assign(value);
    }

    public boolean isComplete(){
        for (Node node: nodes){
            if (!node.isAssignValue()){
                return false;
            }
        }
        return true;
    }

    public boolean isConsistent(){
        for (Node node:nodes){
            if (!node.isConsist()){
                return false;
            }
        }
        return true;
    }

    public boolean isFinalConsistent(){
        for (int i=0 ; i<V ; i++){
            ArrayList<Integer> neighbour = neighbour(i);
            int numberNeighbour = neighbour.size();
            int values[] = new int[numberNeighbour];
            for (int j=0 ; j<numberNeighbour ; j++){
                values[j] = nodes.get(neighbour.get(j)).getValue();
            }

            if (!checkConstraint(numberNeighbour, values, i)){
                return false;
            }
        }
        return true;
    }

    public Graph getCopy(){
        Graph copyGraph = new Graph(getCopyOfNodes(),edges,V,E);
        return copyGraph;
    }

    public ArrayList<Node> getCopyOfNodes(){
        ArrayList<Node> copyNodes = new ArrayList<>();
        for (Node node:nodes){
            copyNodes.add(node.getCopy());
        }
        return copyNodes;
    }

    public void print(){
        for(int i=0 ; i<V ; i++){
            System.out.print(nodes.get(i).getValue() + " ");
        }
        System.out.println("");
    }
}
