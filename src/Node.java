import java.util.ArrayList;
import java.util.Collections;

public class Node {
    private char type; // T, S, P, H, C
    private boolean assignValue;
    private int value;
    private ArrayList<Integer> domain;

    public Node(char type){
        this.type = type;
        domain = new ArrayList<Integer>();
        for(int i=1 ; i<= 9 ; i++){
            domain.add(i);
        }
        value = -1;
        assignValue = false;
    }

    public Node(char type, boolean assignValue, int value, ArrayList<Integer> domain) {
        this.type = type;
        this.assignValue = assignValue;
        this.value = value;
        this.domain = domain;
    }

    public boolean isConsist(){
        return (domain.size()>0 || assignValue);
    }

    public void setDomain(ArrayList<Integer> domain) {
        Collections.sort(domain);
        this.domain = domain;
    }

    public char getType(){
        return type;
    }

    public ArrayList<Integer> getDomain() {
        return domain;
    }

    public int getValue(){
        return value;
    }

    public void assign(int value){
        this.value = value;
        assignValue = true;
    }

    public boolean isAssignValue() {
        return assignValue;
    }

    public int numberDomainRemain(){
        if (assignValue){
            return Integer.MAX_VALUE;
        }
        else{
            return domain.size();
        }
    }

    public Node getCopy(){
        ArrayList<Integer> copyDomain = new ArrayList<Integer>(domain);
        Node copyNode = new Node(type,assignValue,value,copyDomain);

        return copyNode;
    }

}
