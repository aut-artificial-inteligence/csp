import javafx.util.Pair;

import java.util.ArrayList;
import java.util.Scanner;
import java.util.StringTokenizer;

public class CSPSolver {
    private Graph initialGraph;
    private boolean usingMRV;

    public CSPSolver(){
        readInput();
    }

    public void readInput(){
        ArrayList<Node> nodes = new ArrayList<>();
        ArrayList<Pair<Integer,Integer>> edges = new ArrayList<>();
        int V;
        int E;

        Scanner scanner = new Scanner(System.in);
        V = scanner.nextInt();
        E = scanner.nextInt();
        for(int i=0 ; i<V ; i++){
            char typeChar = scanner.next().charAt(0);
            nodes.add(new Node(typeChar));

        }
        for(int i=0 ; i<E ; i++){
            int first = scanner.nextInt();
            int second = scanner.nextInt();
            edges.add(new Pair<>(first,second));
        }
//        V = 9;
//        E = 8;
//        nodes.add(new Node('C'));
//        nodes.add(new Node('P'));
//        nodes.add(new Node('H'));
//        nodes.add(new Node('S'));
//        nodes.add(new Node('S'));
//        nodes.add(new Node('H'));
//        nodes.add(new Node('H'));
//        nodes.add(new Node('T'));
//        nodes.add(new Node('C'));
//        edges.add(new Pair<>(0,1));
//        edges.add(new Pair<>(1,2));
//        edges.add(new Pair<>(2,3));
//        edges.add(new Pair<>(3,4));
//        edges.add(new Pair<>(4,5));
//        edges.add(new Pair<>(5,6));
//        edges.add(new Pair<>(6,7));
//        edges.add(new Pair<>(7,8));

//        V = 3;
//        E = 2;
//        nodes.add(new Node('H'));
//        nodes.add(new Node('C'));
//        nodes.add(new Node('C'));
//
//        edges.add(new Pair<>(0,1));
//        edges.add(new Pair<>(0,2));


//        V = 5;
//        E = 9;
//        nodes.add(new Node('H'));
//        nodes.add(new Node('T'));
//        nodes.add(new Node('T'));
//        nodes.add(new Node('T'));
//        nodes.add(new Node('T'));
//        edges.add(new Pair<>(0,1));
//        edges.add(new Pair<>(0,2));
//        edges.add(new Pair<>(0,4));
//        edges.add(new Pair<>(1,2));
//        edges.add(new Pair<>(1,3));
//        edges.add(new Pair<>(1,4));
//        edges.add(new Pair<>(2,3));
//        edges.add(new Pair<>(2,4));
//        edges.add(new Pair<>(3,4));


        initialGraph = new Graph(nodes,edges,V,E);
    }

    public void solve(boolean usingMRV){
        this.usingMRV = usingMRV;
        Graph answerGraph = backtrack(initialGraph);
        if (answerGraph == null){
            fail();
        }
        else{
            answer(answerGraph);
        }
    }

    public Graph backtrack(Graph parentGraph){
        if (parentGraph.isComplete()){
            if (parentGraph.isFinalConsistent()){
                return parentGraph;
            }
            else {
                return null;
            }
        }

        int var;
        if (usingMRV){
            var = parentGraph.MRV();
        }
        else{
            var = parentGraph.normal();
        }



        ArrayList<Integer> varDomain = parentGraph.domainVar(var);
        for (Integer value : varDomain){
            Graph childGraph = parentGraph.getCopy();
            childGraph.assignVar(var,value);
            childGraph.FC(var);
            if (childGraph.isConsistent()){
                Graph result = backtrack(childGraph);
                if (result != null){
                    return result;
                }
            }
        }
        return null;
    }


    public void answer(Graph answerGraph){
        answerGraph.print();
    }

    public void fail(){
        System.out.println("CSP has not answer!");
    }
}
